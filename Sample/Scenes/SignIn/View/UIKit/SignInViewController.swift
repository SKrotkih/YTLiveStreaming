//
//  SignInViewController.swift
//  LiveEvents
//
//  Created by Serhii Krotkykh
//

import UIKit
import GoogleSignIn

// [START viewcontroller_interfaces]
class SignInViewController: BaseViewController {
    // [END viewcontroller_interfaces]

    @Lateinit var viewModel: SignInViewModel

    // [START viewcontroller_vars]
    @IBOutlet weak var signInButton: GIDSignInButton!
    // [END viewcontroller_vars]

    // [START viewdidload]
    override func viewDidLoad() {
        super.viewDidLoad()

        needNavBarHideShowControl = true
        customizeSignInButton()
    }
    // [END viewdidload]

    @IBAction func signIn(_ sender: Any) {
        viewModel.signIn()
    }
}

// MARK: - Private Methods

extension SignInViewController {
    private func customizeSignInButton() {
        signInButton.style = .standard
        signInButton.colorScheme = .dark
    }
}
