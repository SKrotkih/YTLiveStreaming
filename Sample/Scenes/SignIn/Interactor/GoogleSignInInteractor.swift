//
//  GoogleSignInInteractor.swift
//  LiveEvents
//
//  Created by Serhii Krotkykh
//

import Foundation
import GoogleSignIn
import RxSwift
import Combine

public class GoogleSignInInteractor: NSObject, SignInInteractable {

    enum ReactiveFramework {
        case rxSwift
        case combine
    }

    private let framework: ReactiveFramework = .combine

    let rxSignInResultPublisher: PublishSubject<Result<Void, LVError>> = PublishSubject()
    let signInResultPublisher = CurrentValueSubject<Bool, LVError>(false)
    let logOutPublisher = CurrentValueSubject<Bool, Never>(false)

    @Lateinit var configurator: SignInConfigurator
    @Lateinit var presenter: UIViewController
    @Lateinit var model: SignInStorage

    enum SignInError: Error {
        case signInError(Error)
        case userIsUndefined
        case permissionsError
        case failedUserData

        func localizedString() -> String {
            switch self {
            case .signInError:
                return "The user has not signed in before or he has since signed out"
            case .userIsUndefined:
                return "The user has not signed in before or he has since signed out"
            case .permissionsError:
                return "Please add scopes to have ability to manage your YouTube videos. The app will not work properly"
            case .failedUserData:
                return "User data is wrong. Please try again later"
            }
        }
    }

    // Retrieving user information
    func signIn() {
        // https://developers.google.com/identity/sign-in/ios/people#retrieving_user_information
        GIDSignIn.sharedInstance.signIn(with: configurator.signInConfig,
                                        presenting: presenter) { [weak self] user, error in
            guard let `self` = self else { return }
            if self.framework == .rxSwift {
                self.rxHandleSignInResult(user, error)
            } else {
                self.handleSignInResult(user, error)
            }
        }
    }

    func logOut() {
        GIDSignIn.sharedInstance.signOut()
        model.deleteUserAccaunt()
        logOutPublisher.send(true)
    }

    // It is highly recommended that you provide users that signed in with Google the
    // ability to disconnect their Google account from your app. If the user deletes their account,
    // you must delete the information that your app obtained from the Google APIs.
    func disconnect() {
        GIDSignIn.sharedInstance.disconnect { error in
            guard error == nil else { return }
            // Google Account disconnected from your app.
            // Perform clean-up actions, such as deleting data associated with the
            //   disconnected account.
            self.logOut()
        }
    }
}

// MARK: - Google Sign In Handler

extension GoogleSignInInteractor {
    // [START signin_handler]
    // RxSwift - based method
    private func rxHandleSignInResult(_ user: GIDGoogleUser?, _ error: Error?) {
        do {
            try self.parseSignInResult(user, error)
            self.rxSignInResultPublisher.onNext(.success(Void()))
        } catch SignInError.signInError(let error) {
            if (error as NSError).code == GIDSignInError.hasNoAuthInKeychain.rawValue {
                self.rxSignInResultPublisher.onNext(.failure(.systemMessage(401, SignInError.signInError(error).localizedString())))
            } else {
                self.rxSignInResultPublisher.onNext(.failure(.message(error.localizedDescription)))
            }
        } catch SignInError.userIsUndefined {
            self.rxSignInResultPublisher.onNext(.failure(.systemMessage(401, SignInError.userIsUndefined.localizedString())))
        } catch SignInError.permissionsError {
            self.rxSignInResultPublisher.onNext(.failure(.systemMessage(501, SignInError.permissionsError.localizedString())))
        } catch SignInError.failedUserData {
            self.rxSignInResultPublisher.onNext(.failure(.message(SignInError.failedUserData.localizedString())))
        } catch {
            fatalError("Unexpected exception")
        }
    }

    // Combine (native iOS SDK Framework) - based method
    private func handleSignInResult(_ user: GIDGoogleUser?, _ error: Error?) {
        do {
            try self.parseSignInResult(user, error)
            self.signInResultPublisher.send(true)
        } catch SignInError.signInError(let error) {
            if (error as NSError).code == GIDSignInError.hasNoAuthInKeychain.rawValue {
                self.signInResultPublisher.send(completion: .failure(.systemMessage(401, SignInError.signInError(error).localizedString())))
            } else {
                self.signInResultPublisher.send(completion: .failure(.message(error.localizedDescription)))
            }
        } catch SignInError.userIsUndefined {
            self.signInResultPublisher.send(completion: .failure(.systemMessage(401, SignInError.userIsUndefined.localizedString())))
        } catch SignInError.permissionsError {
            self.signInResultPublisher.send(completion: .failure(.systemMessage(501, SignInError.permissionsError.localizedString())))
        } catch SignInError.failedUserData {
            self.signInResultPublisher.send(completion: .failure(.message(SignInError.failedUserData.localizedString())))
        } catch {
            fatalError("Unexpected exception")
        }
    }

    private func parseSignInResult(_ user: GIDGoogleUser?, _ error: Error?) throws {
        if let error = error {
            throw SignInError.signInError(error)
        } else if user == nil {
            throw SignInError.userIsUndefined
        } else if let user = user, checkPermissions(for: user) {
            if !model.createUserAccaunt(as: user) {
                throw SignInError.failedUserData
            }
        } else {
            throw SignInError.permissionsError
        }
    }
    // [END signin_handler]
}

// MARK: - Check/Add the Scopes

extension GoogleSignInInteractor {
    fileprivate struct Auth {
        // There are needed sensitive scopes to have ability to work properly
        // Make sure they are presented in your app. Then send request on verification
        static let scope1 = "https://www.googleapis.com/auth/youtube"
        static let scope2 = "https://www.googleapis.com/auth/youtube.readonly"
        static let scope3 = "https://www.googleapis.com/auth/youtube.force-ssl"
        static let scopes = [scope1, scope2, scope3]
    }

    private func checkPermissions(for user: GIDGoogleUser) -> Bool {
        guard let grantedScopes = user.grantedScopes else { return false }
        let currentScopes = grantedScopes.compactMap { $0 }
        let havePermissions = currentScopes.contains(where: { Auth.scopes.contains($0) })
        return havePermissions
    }
    
    func addPermissions() {
        // Your app should be verified already!
        GIDSignIn.sharedInstance.addScopes(Auth.scopes,
                                           presenting: self.presenter,
                                           callback: { [weak self] user, error in
            self?.rxHandleSignInResult(user, error)
        })
    }
}
