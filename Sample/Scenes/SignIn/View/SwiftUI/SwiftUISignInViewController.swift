//
//  SwiftUISignInViewController.swift
//  LiveEvents
//
//  Created by Serhii Krotkykh
//

import UIKit

// [START viewcontroller_interfaces]
class SwiftUISignInViewController: BaseViewController {
    // [END viewcontroller_interfaces]

    @Lateinit var viewModel: SignInViewModel

    override func viewDidLoad() {
        super.viewDidLoad()

        needNavBarHideShowControl = true
        addBodyView()
    }

    private func addBodyView() {
        let bodyView = SignInBodyView(viewModel: viewModel)
        addSwiftUIbodyView(bodyView, to: view)
    }
}
