//
//  HomeViewModelProtocols.swift
//  LiveEvents
//
//  Created by Serhii Krotkykh on 27.10.2020.
//  Copyright © 2020 Serhii Krotkykh. All rights reserved.
//

import Foundation
import RxSwift
import Combine

protocol HomeViewModelOutput {
    func didOpenViewAction()
    func didCloseViewAction()
    func didUserLogOutAction()
    func createBroadcast()
    func didLaunchStreamAction(indexPath: IndexPath, viewController: UIViewController)
}

protocol HomeViewModelInput {
    var logOutPublisher: CurrentValueSubject<Bool, Never> { get }
    var errorPublisher: CurrentValueSubject<String, Never> { get }
    var rxData: PublishSubject<[SectionModel]> { get }
}

protocol LogOutExecutable {
    // Input
    var logOutPublisher: CurrentValueSubject<Bool, Never> { get }
    // Output
    func logOut()
}

extension GoogleSignInInteractor: LogOutExecutable {
}
