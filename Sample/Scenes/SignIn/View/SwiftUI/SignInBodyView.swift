//
//  SignInBodyView.swift
//  LiveEvents
//
//  Created by Serhii Krotkykh
//

import SwiftUI
import GoogleSignInSwift

/// SwiftUI content view for the Google Sign In
struct SignInBodyView: View {
    var viewModel: SignInViewModel

    var body: some View {
        VStack {
            Spacer()
            Image("icon-logo")
                .resizable()
                .scaledToFit()
            Spacer()
            // https://developers.google.com/identity/sign-in/ios/sign-in#4_add_a_google_sign-in_button
            GoogleSignInButton(scheme: .dark,
                               style: .standard,
                               action: {
                viewModel.signIn()
            })
            .padding()
            .frame(width: 100.0)

            Spacer()
        }
        .padding(.top, 80.0)
        .padding(.bottom, 80.0)
    }
}

struct SignInBodyView_Previews: PreviewProvider {
    static var previews: some View {
        let interactor = GoogleSignInInteractor()
        let viewModel = GoogleSignInViewModel(interactor: interactor)
        SignInBodyView(viewModel: viewModel)
            .previewDevice(PreviewDevice(rawValue: "iPhone 12 Pro"))
            .previewDisplayName("iPhone 12 Pro")
    }
}
