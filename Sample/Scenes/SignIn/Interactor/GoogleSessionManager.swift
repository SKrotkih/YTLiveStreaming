//
//  GoogleSessionManager.swift
//  LiveEvents
//
//  Created by Serhii Krotkykh
//

import UIKit
import Combine

class GoogleSessionManager: SessionManager {
    @Lateinit var logOutExecutor: LogOutExecutable

    var logOutPublisher: CurrentValueSubject<Bool, Never> {
        return logOutExecutor.logOutPublisher
    }

    init(interactor: LogOutExecutable) {
        self.logOutExecutor = interactor
    }

    func logOut() {
        logOutExecutor.logOut()
    }
}
