//
//  SignInProtocols.swift
//  LiveEvents
//
//  Created by Serhii Krotkykh on 10/31/20.
//  Copyright © 2020 Serhii Krotkykh. All rights reserved.
//

import Foundation
import RxSwift
import Combine
import GoogleSignIn

typealias SignInViewModel = SignInInOutput & SignInInput

protocol SignInInOutput {
    func signInResultListener()
}

protocol SignInInput {
    func signIn()
}

typealias SignInInteractable = SignInObservable & SignInConfiguarble & SignInLaunched

protocol SignInConfiguarble {
    var configurator: SignInConfigurator { get set }
    var presenter: UIViewController { get set }
}

protocol SignInObservable {
    var rxSignInResultPublisher: PublishSubject<Result<Void, LVError>> { get }
    var signInResultPublisher: CurrentValueSubject<Bool, LVError> { get }
}

protocol SignInLaunched {
    func signIn()
    func logOut()
    func disconnect()
    func addPermissions()
}

// MARK: - Model's protocols

typealias SignInStorage = SighInDelegate & UserProfile & Authenticatable

protocol SighInDelegate {
    func createUserAccaunt(as user: GIDGoogleUser) -> Bool
    func deleteUserAccaunt()
}

protocol UserProfile {
    var userName: String { get }
    var userInfo: String { get }
}

protocol Authenticatable {
    var authIdToken: String? { get }
    var authAccessToken: String? { get }
}
