//
//  AppRouter.swift
//  LiveEvents
//
//  Created by Serhii Krotkykh
//

import Foundation
import YTLiveStreaming

let Router = AppDelegate.shared.appRouter

class AppRouter: NSObject {

    enum StroyboadType: String, Iteratable {
        case main = "Main"
        var filename: String {
            return rawValue.capitalized
        }
    }

    // Google SignIn screen
    func showSignInScreen() {
        DispatchQueue.performUIUpdate {
            if #available(iOS 13.0, *) {
                UIStoryboard.main.segueToRootViewController(self.swuftUiSignInDependencies)
            } else {
                UIStoryboard.main.segueToRootViewController(self.signInDependencies)
            }
        }
    }

    // Home screen
    func showHomeScreen() {
        DispatchQueue.performUIUpdate {
            UIStoryboard.main.sequePushViewController(self.homeScreenDependencies)
        }
    }

    // Start Live Video
    func showLiveVideoScreen() {
        DispatchQueue.performUIUpdate {
            UIStoryboard.main.segueToModalViewController(self.liveVideoDependencies, optional: nil)
        }
    }

    // Start Live Video
    func showYouTubeVideoPlayer(videoId: String) {
        DispatchQueue.performUIUpdate {
            if #available(iOS 13.0, *) {
                // Use the Video player UI designed with using SwiftUI
                if let window = AppDelegate.shared.window {
                    let viewController = SwiftUiVideoPlayerViewController()
                    self.swiftUiVideoPlayerDependencies(viewController, videoId)
                    window.rootViewController?.present(viewController, animated: false, completion: {})
                }
            } else {
                // Use the Video player UI designed with using UIKit
                UIStoryboard.main.segueToModalViewController(self.videoPlayerDependencies, optional: videoId)
            }
        }
    }

    // New stream
    func showNewStreamScreen() {
        DispatchQueue.performUIUpdate {
            UIStoryboard.main.sequePushViewController(self.newStreamDependencies)
        }
    }
}

// MARK: - Dependencies Injection

extension AppRouter {
    ///
    /// Inject dependecncies in the SignInViewController
    ///
    private func signInDependencies(_ viewController: SignInViewController) {
        viewController.viewModel = signInSetUpViewModelDependencies(viewController)
    }

    private func swuftUiSignInDependencies(_ viewController: SwiftUISignInViewController) {
        viewController.viewModel = signInSetUpViewModelDependencies(viewController)
    }

    private func signInSetUpViewModelDependencies(_ viewController: UIViewController) -> GoogleSignInViewModel {
        return GoogleSignInViewModel(interactor: createSignInInteractor(viewController))
    }

    private func createSignInInteractor(_ viewController: UIViewController) -> GoogleSignInInteractor {
        let interactor = GoogleSignInInteractor()
        interactor.configurator = GoogleConfigStorage()
        interactor.presenter = viewController
        interactor.model = SignInModel()
        return interactor
    }

    ///
    /// Inject dependecncies in the HomeViewController
    ///
    private func homeScreenDependencies(_ viewController: HomeViewController) {
        let signInSession = GoogleSessionManager(interactor: createSignInInteractor(viewController))

        let viewModel = HomeViewModel()
        let dataSource = HomeScreenDataSource()
        let broadcastsAPI = YTLiveStreaming()
        dataSource.broadcastsAPI = broadcastsAPI
        viewModel.dataSource = dataSource
        viewModel.sessionManager = signInSession

        // Inbound Broadcast
        viewController.output = viewModel
        viewController.input = viewModel
        viewController.userProfile = SignInModel()
    }

    ///
    /// Inject dependecncies in the LFLiveViewController
    ///
    private func liveVideoDependencies(_ viewController: LFLiveViewController, _ optional: Any?) {
        let viewModel = LiveStreamingViewModel()
        let broadcastsAPI = YTLiveStreaming()

        viewModel.broadcastsAPI = broadcastsAPI
        viewController.viewModel = viewModel
    }
    ///
    /// Inject dependecncies in the LFLiveViewController
    ///
    private func newStreamDependencies(_ viewController: NewStreamViewController) {
        let viewModel = NewStreamViewModel()
        let broadcastsAPI = YTLiveStreaming()

        viewModel.broadcastsAPI = broadcastsAPI
        viewController.viewModel = viewModel
    }
    /// UIKit:
    /// Inject dependecncies in to the VideoPlayerViewController
    ///
    private func videoPlayerDependencies(_ viewController: VideoPlayerViewController, _ optional: Any?) {
        VideoPlayerConfigurator.configure(viewController, optional)
    }
    /// SwiftUI:
    /// Inject dependecncies in to the (SwiftUI version of the VideoPlayerViewController) SwiftUiVideoPlayerViewController
    ///
    private func swiftUiVideoPlayerDependencies(_ viewController: SwiftUiVideoPlayerViewController, _ optional: Any?) {
        SwiftUIVideoPlayerConfigurator.configure(viewController, optional)
    }
}

extension AppRouter {
    func closeModal(viewController: UIViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UIApplicationDelegate protocol.

extension AppRouter: UIApplicationDelegate {
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        showSignInScreen()
        return true
    }
}
