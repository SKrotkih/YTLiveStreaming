//
//  HomeViewModel.swift
//  LiveEvents
//
//  Created by Serhii Krotkykh
//

import Foundation
import YTLiveStreaming
import RxSwift
import Combine

enum VideoPlayerType {
    case oldVersionForIos8
    case AVPlayerViewController
    case VideoPlayerViewController
}

class HomeViewModel: HomeViewModelOutput {
    // Default value of the used video player
    private static let playerType: VideoPlayerType = .VideoPlayerViewController

    @Lateinit var dataSource: BroadcastsDataFetcher
    @Lateinit var sessionManager: SessionManager

    var errorPublisher = CurrentValueSubject<String, Never>("")
    private let disposeBag = DisposeBag()

    lazy private var videoPlayer = YouTubePlayer()

    var playerFactory: YouTubeVideoPlayed {
        switch HomeViewModel.playerType {
        case .oldVersionForIos8:
            return XCDYouTubeVideoPlayer8()
        case .AVPlayerViewController:
            return XCDYouTubeVideoPlayer()
        case .VideoPlayerViewController:
            return YTVideoPlayer()
        }
    }

    func didOpenViewAction() {
        configure()
        dataSource.loadData()
    }

    func didUserLogOutAction() {
        sessionManager.logOut()
    }

    func didCloseViewAction() {
        Router.showSignInScreen()
    }

    private func configure() {
        rxData
            .subscribe(onNext: { data in
                var message = ""
                data.forEach { item in
                    if let errorMessage = item.error {
                        message += errorMessage + "\n"
                    }
                }
                if !message.isEmpty {
                    self.errorPublisher.send(message)
                }
            }).disposed(by: disposeBag)
    }

    func createBroadcast() {
        Router.showNewStreamScreen()
    }

    func didLaunchStreamAction(indexPath: IndexPath, viewController: UIViewController) {
        videoPlayer.youtubeVideoPlayer = playerFactory

        switch indexPath.section {
        case 0:
            assert(false, "Incorrect section number")
        case 1:
            let broadcast = dataSource.getCurrent(for: indexPath.row)
            videoPlayer.playYoutubeID(broadcast.id, viewController: viewController)
        case 2:
            let broadcast = dataSource.getPast(for: indexPath.row)
            videoPlayer.playYoutubeID(broadcast.id, viewController: viewController)
        default:
            assert(false, "Incorrect section number")
        }
    }
}

// MARK: - HomeViewModelInput protocol implementation

extension HomeViewModel: HomeViewModelInput {
    var logOutPublisher: CurrentValueSubject<Bool, Never> {
        return sessionManager.logOutPublisher
    }

    var rxData: PublishSubject<[SectionModel]> {
        return dataSource.rxData
    }
}
