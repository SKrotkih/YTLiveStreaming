//
//  SignInModel.swift
//  LiveEvents
//
//  Created by Serhii Krotkih on 6/14/22.
//

import Foundation
import GoogleSignIn
import YTLiveStreaming

// User profile information

struct GoogleUser: Codable {
    let userId: String
    let idToken: String
    let accessToken: String?
    let fullName: String
    let givenName: String
    let familyName: String
    let profilePicUrl: URL?
    let email: String

    init?(_ user: GIDGoogleUser) {
        if let userId = user.userID,
           let idToken = user.authentication.idToken {
            self.userId = userId
            self.idToken = idToken
            accessToken = user.authentication.accessToken
            fullName = user.profile?.name ?? ""
            givenName = user.profile?.givenName ?? ""
            familyName = user.profile?.familyName ?? ""
            profilePicUrl = user.profile?.imageURL(withDimension: 320)
            email = user.profile?.email ?? ""
        } else {
            return nil
        }
    }

    static var keyName: String {
        return String(describing: self)
    }
}

class SignInModel: SignInStorage {
    private let userKey = GoogleUser.keyName
    private var _currentUser: GoogleUser?
    private var currentUser: GoogleUser? {
        if _currentUser == nil {
            _currentUser = LocalStorage.restoreObject(key: userKey)
            // OR we can use this:
            if let currentGoogleUser = GIDSignIn.sharedInstance.currentUser {
                _currentUser = GoogleUser(currentGoogleUser)
            }
        }
        return _currentUser
    }

    var userName: String {
        return currentUser?.fullName ?? "undefined"
    }

    var userInfo: String {
        return currentUser?.givenName ?? "info is not presented"
    }

    var authIdToken: String? {
        return currentUser?.idToken
    }

    var authAccessToken: String? {
        return currentUser?.accessToken
    }

    func createUserAccaunt(as user: GIDGoogleUser) -> Bool {
        // save data
        if let user = GoogleUser(user), LocalStorage.saveObject(user, key: userKey) {
            _currentUser = user
            GoogleOAuth2.sharedInstance.accessToken = authAccessToken
            return true
        }
        return false
    }

    func deleteUserAccaunt() {
        // clean data
        _currentUser = nil
        GoogleOAuth2.sharedInstance.clearToken()
        LocalStorage.removeObject(key: userKey)
    }
}
