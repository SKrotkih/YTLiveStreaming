//
//  SignInViewModel.swift
//  LiveEvents
//
//  Created by Serhii Krotkykh
//

import Foundation
import RxSwift
import Combine

class GoogleSignInViewModel: SignInViewModel {
    @Lateinit var interactor: SignInInteractable

    private let disposeBag = DisposeBag()
    private var cancellableBag = Set<AnyCancellable>()

    required init(interactor: SignInInteractable) {
        self.interactor = interactor
        signInResultListener()
    }

    // MARK: - SignInInput

    func signIn() {
        interactor.signIn()
    }

    func requestPermissions() {
        interactor.addPermissions()
    }

    // MARK: - SignInInOutput

    func signInResultListener() {
        // The RxSwift based subscriber
        interactor
            .rxSignInResultPublisher
            .subscribe(onNext: { result in
                self.parse(result)
            }).disposed(by: disposeBag)

        // The Combine based subscriber
        interactor
            .signInResultPublisher
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    self.parse(error: error)
                default:
                    break
                }
            }, receiveValue: { theUserIsLoggedIn in
                if theUserIsLoggedIn {
                    Router.showHomeScreen()
                }
            })
            .store(in: &cancellableBag)
    }

    // MARK: - Private methods

    private func parse(_ result: Result<Void, LVError>) {
        switch result {
        case .success:
            Router.showHomeScreen()
        case .failure(let error):
            // in case we get error return to the signin screen
            interactor.disconnect()
            parse(error: error)
        }
    }
    
    private func parse(error: LVError) {
        switch error {
        case .systemMessage(let code, let message):
            switch code {
            case 401:
                print(message)
            case 501:
                Alert.showOkCancel(message, message: "Do you want to send request?", onComplete: {
                    self.requestPermissions()
                })
            default:
                Alert.showOk("", message: message)
            }
        case .message(let message):
            Alert.showOk("", message: message)
        }
    }
}
